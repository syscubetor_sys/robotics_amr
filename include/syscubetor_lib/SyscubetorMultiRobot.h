#ifndef __SYSCUBETOR_MULTIROBOT_CLASS_H__
#define __SYSCUBETOR_MULTIROBOT_CLASS_H__

#pragma once

#include <thread>
#include <mutex>
#include <unistd.h>
#include <geometry_msgs/Pose.h>
#include <syscubetor_lib/SyscubetorBase.h>
#include <robotics_amr/SyscubetorMultiRobotMessage.h>

using namespace std;

namespace robotics_amr
{
class SyscubetorMultiRobot 
{
private:
	int32_t number_of_robots;
	vector<string> robot_namespaces;
	vector<SyscubetorBase> goal_send_handler_pool;
	vector<bool> goal_results;
	mutex goal_handling_mutex;
	vector<thread> multi_robot_thread_pool;

	void setGoalResults(int32_t goal_idx,bool result)
	{
		lock_guard<mutex> guard(goal_handling_mutex);
		goal_results[goal_idx] = result;
	}

	vector<bool> getGoalResults() { return goal_results; }
public:
	SyscubetorMultiRobot(int32_t num_robots,vector<string> robot_namespaces)
	{
		this->number_of_robots = num_robots;
		this->robot_namespaces = robot_namespaces; 
		goal_results.reserve(num_robots);

		for(int32_t cnt = 0 ; cnt < this->number_of_robots; cnt++)
		{
			goal_send_handler_pool.push_back(SyscubetorBase(robot_namespaces[cnt].c_str()));
		}

		ROS_INFO("Created multiple robot action client instances");
	}

	~SyscubetorMultiRobot()
	{

	}

	void goalSendingThread(SyscubetorBase &base_goal_handler,
							robotics_amr::SyscubetorMultiRobotMessage goal_message,string robot_namespace,int32_t goal_index);

	void sendMultipleGoals(vector<robotics_amr::SyscubetorMultiRobotMessage> multi_robot_goals);

	vector<bool> fetchGoalResults();

};
}


#endif